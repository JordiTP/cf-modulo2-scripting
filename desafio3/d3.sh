#!/bin/bash

# Get the functions
source $PWD/d3.functions.sh

# Get the environment variables
if [ -f .env ]; then
    # Load the environment variables
    source $PWD/.env
    # Check if script is running as root
    check_root
else
    # Check if script is running as root
    check_root
    # Edit the configuration file
    check_env
    # Load the environment variables
    source $PWD/.env
fi

echo "---------------------------------------------------------------------"
echo " "
echo " >>> Challenge 3 - CF-DevOps-Roxs"
echo " >>> Deploy a store book with NodeJS and PM2"
echo " >>> Distibution: $(lsb_release -d | awk '{print $2, $3}')"
echo " "
echo "---------------------------------------------------------------------"
sleep 2

# Cleanup execution enable
chmod +x $PWD/cleanup.sh

# Verify the requirements - Install NodeJS
verify_requeriments

# Install PM2
install_pm2

# Clone the repository
repository_clone

# Install App requirements
app_requirements

# Configure PM2
configure_pm2

# Install Nginx
install_nginx

# Configure Nginx
configure_nginx

# End of the script
echo "---------------------------------------------------------------------"
echo " "
echo " >>> Finished the script of Challenge 3 - CF-DevOps-Roxs"
echo " "
echo " >>> Access the app $APP_NAME: "
echo "     Local: http://localhost:3000/"
echo "     Public: http://$(hostname -I | sed 's/ $//'):3000/"
echo " "
echo " >>> Access the app $APP_NAME (Reverse Proxy): "
echo "     Local: http://localhost/"
echo "     Public: http://$(hostname -I | sed 's/ $//')/"
echo " "
echo "---------------------------------------------------------------------"
sleep 2
