#!/bin/bash

# Get the functions
source $PWD/d3.functions.sh
source $PWD/.env

# Check if script is running as root
check_root

echo "---------------------------------------------------------------------"
echo " "
echo " >>> Challenge 3 - CF-DevOps-Roxs"
echo " >>> Cleanup the environment - Remove NodeJS, PM2, Nginx and App"
echo " >>> Distibution: $(lsb_release -d | awk '{print $2, $3}')"
echo " "
echo "---------------------------------------------------------------------"
sleep 2

# PM2 cleanup
cleanup_pm2

# NodeJS cleanup
cleanup_nodejs

# Nginx cleanup
cleanup_nginx

# App cleanup
cleanup_app

# Remove the environment file
rm -rf $PWD/.env

# End of the script
echo "---------------------------------------------------------------------"
echo " "
echo " >>> Finished the cleanup of Challenge 3 - CF-DevOps-Roxs"
echo " "
echo "---------------------------------------------------------------------"
sleep 2
