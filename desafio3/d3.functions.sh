#!/bin/bash

check_root() {
    if [ "$(id -u)" -eq 0 ]; then
        echo " "
        echo "---------------------------------------------------------------------"
        echo ">>> Running as root."
        echo "---------------------------------------------------------------------"
    elif pgrep -s 0 '^sudo$' > /dev/null ; then
        echo " "
        echo "---------------------------------------------------------------------"
        echo ">>> Running with sudo."
        echo "---------------------------------------------------------------------"
    else
        echo " "
        echo "---------------------------------------------------------------------"
        echo ">>> ERROR: This script must be run as root or with sudo. Exiting..."
        echo "---------------------------------------------------------------------"
        exit 1
    fi
}

edit_env() {
    echo "---------------------------------------------------------------------"
    read -p ">>> Do you want to edit the configuration file? (y/n): " choice
    echo "---------------------------------------------------------------------"
    if [ "$choice" == "y" ]; then
        read -p ">>> Which editor do you want to use? (vi/nano): " editor
        if [ "$editor" == "vi" ]; then
            if ! command -v vi &> /dev/null; then
                echo ">>> vi is not installed. Installing..."
                apt-get update
                apt-get install -y vim
            fi
            vi .env
        elif [ "$editor" == "nano" ]; then
            if ! command -v nano &> /dev/null; then
                echo ">>> nano is not installed. Installing..."
                apt-get update
                apt-get install -y nano
            fi
            nano .env
        else
            echo ">>> Invalid editor choice."
        fi
    fi
}

get_random() {
    local result=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w $1 | head -n 1)
    echo $result
}

replace_inside() {
    local result=$(sed -i "s/$1/$2/g" $3)
    echo $result
}

check_env() {
    if [ -f .env ]; then
        edit_env
    else
        echo " "
        echo "---------------------------------------------------------------------"
        echo " >>> Copying the configuration file (.env.sample to .env)"
        echo "---------------------------------------------------------------------"
        
        # Generate random values
        RANDOM_USER='wp_user_'$(get_random 4)
        RANDOM_PASSWORD=$(get_random 12)
        replace_inside "wp_user" $RANDOM_USER ".env.sample"
        replace_inside "wp_temp_pass" $RANDOM_PASSWORD ".env.sample"

        # Copy the configuration file
        cp .env.sample .env
        edit_env
    fi
}

install_nodejs() {
    echo " "
    echo "---------------------------------------------------------------------"
    echo " >>> Installing NodeJS on Ubuntu..."
    echo "---------------------------------------------------------------------"
    apt-get update

    # Download the NodeJS v16 tar file
    wget https://nodejs.org/download/release/v16.20.2/node-v16.20.2-linux-x64.tar.gz
    
    # Removed because it is more clean to use the /opt/ directory
    #tar -C /usr/local --strip-components 1 -xzf node-v16.20.2-linux-x64.tar.gz
    #ls -l /usr/local/bin/node ls -l /usr/local/bin/npm

    # Extract the tar file to /opt/ directory
    tar -xf node-v16.20.2-linux-x64.tar.gz --directory=/opt/

    # Create a symbolic link to the /opt/node-v16.20.2-linux-x64/bin/ directory
    echo 'export PATH="/opt/node-v16.20.2-linux-x64/bin/:$PATH"' >> ~/.bashrc && source ~/.bashrc

    # Fix to run as sudo
    if [ ! -d "/sbin/node" ]; then
    ln -s /opt/node-v16.20.2-linux-x64/bin/node /sbin/node
    fi
    if [ ! -d "/sbin/npm" ]; then
    ln -s /opt/node-v16.20.2-linux-x64/bin/npm /sbin/npm
    fi
    if [ ! -d "/sbin/npx" ]; then
        ln -s /opt/node-v16.20.2-linux-x64/bin/npx /sbin/npx
    fi

    # Change the owner of the /opt/node-v16.20.2-linux-x64/ directory
    #chown -R $USER:$USER /opt/node-v16.20.2-linux-x64/
    sleep 8

    if [ $? -eq 0 ]; then
        echo " "
        echo "---------------------------------------------------------------------"
        echo " >>> Installed successfully NodeJS - Version: $(node -v)"
        echo "---------------------------------------------------------------------"
    else
        echo " "
        echo "---------------------------------------------------------------------"
        echo " >>> ERROR: NodeJS could not be installed. Exiting..."
        echo "---------------------------------------------------------------------"
        exit 1
    fi    
}

verify_requeriments() {
    if [ "$(node -v)" == "16.20.2" ]; then
        echo " "
        echo "---------------------------------------------------------------------"
        echo " >>> Requirements satisfied:"
        echo " >>> Distibution: $(lsb_release -d | awk '{print $2, $3}')"
        echo " >>> NodeJS: $(node -v)"
        echo " >>> NPM: v$(npm -v)"
        echo "---------------------------------------------------------------------"
    else
        echo " "
        echo "---------------------------------------------------------------------"
        echo " >>> ERROR: Requirements not satisfied. Needs NodeJS 16.20.2 . Installing..."
        echo "---------------------------------------------------------------------"
        install_nodejs
    fi
}

install_pm2() {
    echo " "
    echo "---------------------------------------------------------------------"
    echo " >>> Installing PM2 on Ubuntu..."
    echo "---------------------------------------------------------------------"
    
    # Install PM2
    npm install pm2@latest -g

    if [ $? -eq 0 ]; then
        echo " "
        echo "---------------------------------------------------------------------"
        echo " >>> PM2 Installed successfully - Version: $(npm list -g | awk -F@ '/pm2/ { print $2}')"
        echo "---------------------------------------------------------------------"
    else
        echo " "
        echo "---------------------------------------------------------------------"
        echo " >>> ERROR: PM2 could not be installed. Exiting..."
        echo "---------------------------------------------------------------------"
        exit 1
    fi

    # Start PM2 on boot - pm2 startup
    sudo env PATH=$PATH:/opt/node-v16.20.2-linux-x64/bin /opt/node-v16.20.2-linux-x64/lib/node_modules/pm2/bin/pm2 startup systemd -u $USER --hp /home/    sudo env PATH=$PATH:/opt/node-v16.20.2-linux-x64/bin /opt/node-v16.20.2-linux-x64/lib/node_modules/pm2/bin/pm2 startup systemd -u x64$USER --hp /home/    sudo env PATH=$PATH:/opt/node-v16.20.2-linux-x64/bin /opt/node-v16.20.2-linux-x64/lib/node_modules/pm2/bin/pm2 startup systemd -u $USER --hp /home/$USER
}

repository_clone() {
    # Make the directory where the repository will be cloned
    cd /
    mkdir $APP_REPO
    
    #chown -R $USER:$USER $APP_REPO
    cd $APP_REPO

    if ! command -v nano &> /dev/null; then
        echo ">>> git is not installed. Installing..."
        apt-get update
        apt-get install -y git
    fi
    # Clone the repository
    git clone https://gitlab.com/training-devops-cf/book-store-devops.git
}

app_requirements() {
    # Install the application requirements
    cd $APP_REPO$APP_REPO_PATH
    npm install

    # Fix bugs of missing declared dependencies
    npm i @babel/plugin-transform-private-property-in-object
    npm i @babel/plugin-proposal-private-property-in-object

    if [ $? -eq 0 ]; then
        echo " "
        echo "---------------------------------------------------------------------"
        echo " >>> Application requirements installed successfully."
        echo "---------------------------------------------------------------------"
    else
        echo " "
        echo "---------------------------------------------------------------------"
        echo " >>> ERROR: Application requirements could not be installed. Exiting..."
        echo "---------------------------------------------------------------------"
        exit 1
    fi

    # Build the application
    npm run build
}

configure_pm2() {
    # Create symbolic link - ln -s /app-repo/book-store-devops/book-store /app
    ln -s "$APP_REPO$APP_REPO_PATH/src" $APP_DIR

    # Start the application with PM2
    cd $APP_DIR
    # pm2 start --name book-store npm -- start
    pm2 start --name $APP_NAME npm -- start
}

install_nginx() {
    echo "---------------------------------------------------------------------"
    read -p " >>> Do you want to install Nginx? (y/n): " choice
    echo "---------------------------------------------------------------------"
    if [ "$choice" == "y" ]; then
        echo " "
        echo "---------------------------------------------------------------------"
        echo " >>> Installing Nginx on Ubuntu..."
        echo "---------------------------------------------------------------------"
        
        # Install Nginx
        apt-get update
        apt-get install -y nginx

        if [ $? -eq 0 ]; then
            echo " "
            echo "---------------------------------------------------------------------"
            echo " >>> Nginx installed successfully - Version $(dpkg-query -W -f '${version}\n' nginx)"
            echo "---------------------------------------------------------------------"
        else
            echo " "
            echo "---------------------------------------------------------------------"
            echo " >>> ERROR: Nginx could not be installed. Exiting..."
            echo "---------------------------------------------------------------------"
            exit 1
        fi
    fi    
}

create_reverse_proxy_conf() {
    PUBLICIP = $(hostname -I | sed 's/ $//')
    cat <<EOF > /etc/nginx/sites-available/reverse-proxy.conf
server {
    listen 80;
    listen [::]:80;

    location / {
        proxy_pass http://$PUBLICIP:3000;
        include proxy_params;
    }
}
EOF
}

configure_nginx() {
    # Disable the default virtual host
    unlink /etc/nginx/sites-enabled/default

    # Create the reverse proxy configuration
    create_reverse_proxy_conf

    # Activate the directives
    ln -s /etc/nginx/sites-available/reverse-proxy.conf /etc/nginx/sites-enabled/reverse-proxy.conf

    # Test the Nginx configuration
    service nginx configtest
    if [ $? -eq 0 ]; then
        echo " "
        echo "---------------------------------------------------------------------"
        echo " >>> Nginx configuration test passed."
        echo "---------------------------------------------------------------------"
    else
        echo " "
        echo "---------------------------------------------------------------------"
        echo " >>> ERROR: Nginx configuration test failed. Please check the configuration file..."
        echo "---------------------------------------------------------------------"
        
        read -p ">>> Which editor do you want to use? (vi/nano): " editor
        if [ "$editor" == "vi" ]; then
            if ! command -v vi &> /dev/null; then
                echo ">>> vi is not installed. Installing..."
                apt-get update
                apt-get install -y vim
            fi
            vi /etc/nginx/sites-available/reverse-proxy.conf
        elif [ "$editor" == "nano" ]; then
            if ! command -v nano &> /dev/null; then
                echo ">>> nano is not installed. Installing..."
                apt-get update
                apt-get install -y nano
            fi
            nano /etc/nginx/sites-available/reverse-proxy.conf
        else
            echo ">>> Invalid editor choice."
        fi

        # Test the Nginx configuration
        service nginx configtest
        if [ $? -eq 0 ]; then
            echo " "
            echo "---------------------------------------------------------------------"
            echo " >>> Nginx configuration test passed."
            echo "---------------------------------------------------------------------"
        else
            echo " "
            echo "---------------------------------------------------------------------"
            echo " >>> ERROR: Nginx configuration test failed. Exiting..."
            echo "---------------------------------------------------------------------"
            exit 1
        fi
    fi

    # Restart Nginx
    service nginx restart
}

cleanup_pm2() {
    echo "---------------------------------------------------------------------"
    echo " >>> Removing PM2 ..."
    echo "---------------------------------------------------------------------"
    pm2 stop $APP_NAME
    pm2 delete all
    npm uninstall -g pm2
    if [ $? -eq 0 ]; then
        echo "---------------------------------------------------------------------"
        echo " >>> PM2 has been successfully uninstalled."
        echo "---------------------------------------------------------------------"
    else
        echo "---------------------------------------------------------------------"
        echo " >>> ERROR: PM2 could not be uninstalled."
        echo "---------------------------------------------------------------------"
    fi
    # Remove P2M from the auto start
    export PATH=${PATH/'/opt/node-v16.20.2-linux-x64/bin /opt/node-v16.20.2-linux-x64/lib/node_modules/pm2/bin/pm2 startup systemd -u x64 --hp /home/x64'/}
}

cleanup_nodejs() {
    echo "---------------------------------------------------------------------"
    echo " >>> Removing NodeJS ..."
    echo "---------------------------------------------------------------------"
    # Remove nodejs from the PATH
    export PATH=${PATH/'/opt/node-v16.20.2-linux-x64/bin/'/}

    # Remove the symbolic links
    rm -rf /sbin/node
    rm -rf /sbin/npm
    rm -rf /sbin/npx

    # Remove NodeJS from the /opt/ directory
    rm -rf /opt/node-v16.20.2-linux-x64/

    # Remove the downloaded node file
    rm -rf $PWD/node-v16.20.2-linux-x64.tar.gz
    echo "---------------------------------------------------------------------"
    echo " >>> NodeJS has been successfully uninstalled."
    echo "---------------------------------------------------------------------"
}

cleanup_nginx() {
    echo "---------------------------------------------------------------------"
    echo " >>> Removing Nginx ..."
    echo "---------------------------------------------------------------------"
    systemctl stop nginx
    apt purge -y nginx* libnginx*
    apt remove -y nginx* libnginx*
    rm -rf /etc/nginx /var/lib/nginx /var/log/nginx /usr/share/nginx /var/www
    apt autoremove
    apt autoclean
    echo "---------------------------------------------------------------------"
    echo " >>> Nginx has been successfully uninstalled."
    echo "---------------------------------------------------------------------"
}

cleanup_app() {
    echo "---------------------------------------------------------------------"
    echo " >>> Removing App ..."
    echo "---------------------------------------------------------------------"
    rm -rf /app-repo /app
    echo "---------------------------------------------------------------------"
    echo " >>> App has been successfully deleted."
    echo "---------------------------------------------------------------------"
}
