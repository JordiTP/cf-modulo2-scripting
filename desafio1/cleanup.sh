#!/bin/bash

# Get the functions
source $PWD/d1.functions.sh
source $PWD/.env

# Check if script is running as root
check_root

echo "---------------------------------------------------------------------"
echo " "
echo " >>> Challenge 1 - CF-DevOps-Roxs"
echo " >>> Cleanup the environment - Remove phpMyAdmin"
echo " >>> Distibution: $(lsb_release -d | awk '{print $2, $3}')"
echo " "
echo "---------------------------------------------------------------------"
sleep 2

# Remove phpMyAdmin and required PHP extensions
cleanup_phpmyadmin

# MediaWiki cleanup
cleanup_mediawiki

# Remove the environment file
rm -rf $PWD/.env

# End of the script
echo "---------------------------------------------------------------------"
echo " "
echo " >>> Finished the cleanup of Challenge 1 - CF-DevOps-Roxs"
echo " "
echo "---------------------------------------------------------------------"
sleep 2
