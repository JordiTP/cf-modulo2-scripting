#!/bin/bash

# Get the functions
source $PWD/d1.functions.sh

# Get the environment variables
if [ -f .env ]; then
    # Load the environment variables
    source $PWD/.env
    # Check if script is running as root
    check_root
else
    # Check if script is running as root
    check_root
    # Edit the configuration file
    check_env
    # Load the environment variables
    source $PWD/.env
fi

echo "---------------------------------------------------------------------"
echo " "
echo " >>> Challenge 1 - CF-DevOps-Roxs"
echo " >>> Install phpMyAdmin (MediaWiki required)"
echo " >>> Distibution: $(lsb_release -d | awk '{print $2, $3}')"
echo " "
echo "---------------------------------------------------------------------"
sleep 2

# Cleanup execution enable
chmod +x $PWD/cleanup.sh

# Verify the requirements
verify_requeriments

# Install phpMyAdmin
install_phpmyadmin

# Configure phpMyAdmin
configure_phpmyadmin

# Secure phpMyAdmin
secure_phpmyadmin

# End of the script
echo "---------------------------------------------------------------------"
echo " "
echo " >>> Finished the script of Challenge 1 - CF-DevOps-Roxs"
echo " >>> phpMyAdmin version: $(dpkg-query -W -f '${version}\n' phpmyadmin)"
echo " "
echo " >>> Access phpMyAdmin at: "
echo "     Local: http://localhost/"$PHPMYADMIN_ALIAS
echo "     Public: http://$(hostname -I | sed 's/ $//')/"$PHPMYADMIN_ALIAS
echo " >>> Basic Auth Username: "$PHPMYADMIN_BASICAUTH_USER
echo " >>> Basic Auth Password: "$PHPMYADMIN_BASICAUTH_PASSWORD
echo " "
echo "---------------------------------------------------------------------"
sleep 2
