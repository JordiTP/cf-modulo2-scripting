#!/bin/bash

check_root() {
    if [ "$(id -u)" -eq 0 ]; then
        echo " "
        echo "---------------------------------------------------------------------"
        echo ">>> Running as root."
        echo "---------------------------------------------------------------------"
    elif pgrep -s 0 '^sudo$' > /dev/null ; then
        echo " "
        echo "---------------------------------------------------------------------"
        echo ">>> Running with sudo."
        echo "---------------------------------------------------------------------"
    else
        echo " "
        echo "---------------------------------------------------------------------"
        echo ">>> ERROR: This script must be run as root or with sudo. Exiting..."
        echo "---------------------------------------------------------------------"
        exit 1
    fi
}

edit_env() {
    echo "---------------------------------------------------------------------"
    read -p ">>> Do you want to edit the configuration file? (y/n): " choice
    echo "---------------------------------------------------------------------"
    if [ "$choice" == "y" ]; then
        read -p ">>> Which editor do you want to use? (vi/nano): " editor
        if [ "$editor" == "vi" ]; then
            if ! command -v vi &> /dev/null; then
                echo ">>> vi is not installed. Installing..."
                apt-get update
                apt-get install -y vim
            fi
            vi .env
        elif [ "$editor" == "nano" ]; then
            if ! command -v nano &> /dev/null; then
                echo ">>> nano is not installed. Installing..."
                apt-get update
                apt-get install -y nano
            fi
            nano .env
        else
            echo ">>> Invalid editor choice."
        fi
    fi
}

get_random() {
    local result=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w $1 | head -n 1)
    echo $result
}

replace_inside_env() {
    local result=$(sed -i "s/$1/$2/g" .env.sample)
    echo $result
}

check_env() {
    if [ -f .env ]; then
        edit_env
    else
        echo " "
        echo "---------------------------------------------------------------------"
        echo " >>> Copying the configuration file (.env.sample to .env)"
        echo "---------------------------------------------------------------------"
        
        # Generate random values
        RANDOM_ALIAS='phpmyadmin-'$(get_random 6)
        RANDOM_PASSWORD=$(get_random 10)
        replace_inside_env 'phpmyadmin-r4nd0m' $RANDOM_ALIAS
        replace_inside_env 'pass3587' $RANDOM_PASSWORD

        # Copy the configuration file
        cp .env.sample .env
        edit_env
    fi
}

verify_requeriments() {
    if [ -d "/var/www/html/mediawiki" ] && ! dpkg -l "apache2" > /dev/null 2>&1 && ! dpkg -l "php" > /dev/null 2>&1; then
        echo " "
        echo "---------------------------------------------------------------------"
        echo " >>> Requirements satisfied."
        echo "---------------------------------------------------------------------"
    else
        echo " "
        echo "---------------------------------------------------------------------"
        echo " >>> ERROR: Requirements not satisfied."
        echo "---------------------------------------------------------------------"
        read -p " >>> Do you want to run exercise #7 to install MediaWiki? (y/n): " choice
        echo "---------------------------------------------------------------------"
        if [ "$choice" == "y" ]; then
            # Bug - 7.sh line 14 - dpkg -l apache > /dev/null 2>&1 detects apache2 as installed
            apt-get install -y apache2
            # Bug
            chmod +x $PWD/../ejercicios/7/7.sh
            source $PWD/../ejercicios/7/7.sh
            cd $PWD/../ejercicios/7/7.sh
        else
            echo " "
            echo "---------------------------------------------------------------------"
            echo " >>> ERROR: Requirements not satisfied. Exiting..."
            echo "---------------------------------------------------------------------"
            exit 1
        fi
    fi
}

install_phpmyadmin() {
    echo " "
    echo "---------------------------------------------------------------------"
    echo " >>> Installing phpMyAdmin and required PHP extensions..."
    echo "---------------------------------------------------------------------"
    apt-get update
    # Bug - Little fix for the javascript-common package
    purge javascript-common 
    apt-get install javascript-common
    #
    apt-get install -y phpmyadmin php-zip php-json php-curl
    if [ $? -eq 0 ]; then
        echo " "
        echo "---------------------------------------------------------------------"
        echo " >>> phpMyAdmin installed successfully."
        echo "---------------------------------------------------------------------"
    else
        echo " "
        echo "---------------------------------------------------------------------"
        echo " >>> ERROR: phpMyAdmin could not be installed. Exiting..."
        echo "---------------------------------------------------------------------"
        exit 1
    fi
}

configure_phpmyadmin() {
    echo " "
    echo "---------------------------------------------------------------------"
    echo ">>> Configuring phpMyAdmin..."
    echo "---------------------------------------------------------------------"
    ln -s /etc/phpmyadmin/apache.conf /etc/apache2/conf-available/phpmyadmin.conf
    a2enconf phpmyadmin
    systemctl reload apache2.service
    echo " "
    echo "---------------------------------------------------------------------"
    echo " >>> phpMyAdmin configured successfully."
    echo "---------------------------------------------------------------------"
}

htaccess_enable_basicauth() {
    cat <<EOF > /usr/share/phpmyadmin/.htaccess
AuthType Basic
AuthName "Stay Away"
AuthUserFile /etc/phpmyadmin/.htpasswd
Require valid-user
EOF
}

secure_phpmyadmin() {
    echo " "
    echo "---------------------------------------------------------------------"
    echo ">>> Securing phpMyAdmin..."
    echo "---------------------------------------------------------------------"
    sed -i "s@Alias /phpmyadmin@Alias /$PHPMYADMIN_ALIAS@g" /etc/phpmyadmin/apache.conf
    sed -i "s@DirectoryIndex index.php@DirectoryIndex index.php\n\tAllowOverride All@" /etc/phpmyadmin/apache.conf
    
    echo ">>> Enabling Basic Authentication..."
    htaccess_enable_basicauth

    echo ">>> Creating the .htpasswd file for phpMyAdmin..."
    htpasswd -bc /etc/phpmyadmin/.htpasswd $PHPMYADMIN_BASICAUTH_USER $PHPMYADMIN_BASICAUTH_PASSWORD

    echo ">>> Restarting Apache..."
    systemctl restart apache2
    echo " "
    echo "---------------------------------------------------------------------"
    echo " >>> phpMyAdmin secured successfully."
    echo "---------------------------------------------------------------------"
}

cleanup_phpmyadmin() {
    echo "---------------------------------------------------------------------"
    read -p " >>> Do you want to remove phpMyAdmin? (y/n): " choice
    echo "---------------------------------------------------------------------"
    if [ "$choice" == "y" ]; then
        echo ">>> Removing phpMyAdmin and required PHP extensions..."
        apt purge -y phpmyadmin
        apt autoremove
        apt autoclean
        rm /etc/apache2/conf-available/phpmyadmin.conf
        rm -rf /etc/phpmyadmin /usr/share/phpmyadmin /var/lib/phpmyadmin /usr/share/phpmyadmin
        rm .env
        systemctl reload apache2.service
    fi
}

cleanup_mediawiki() {
    echo "---------------------------------------------------------------------"
    read -p " >>> Do you want to remove MediaWiki (from exercise #7)? (y/n): " choice
    echo "---------------------------------------------------------------------"
    if [ "$choice" == "y" ]; then
        systemctl stop apache2
        a2dissite mediawiki.conf
        a2ensite 000-default
        systemctl start apache2
        rm -rf /var/www/html/mediawiki /etc/apache2/sites-available/mediawiki.conf
        rm $PWD/mediawiki-1.41.0.tar.gz*

        echo "---------------------------------------------------------------------"
        read -p " >>> Do you want to remove apache and php? (y/n): " choice
        echo "---------------------------------------------------------------------"
        if [ "$choice" == "y" ]; then
            systemctl stop apache2
            apt purge -y apache2* libapache2* php*
            rm -rf /etc/apache2 /var/lib/apache2 /var/log/apache2 /usr/share/apache2 /var/www
            rm -rf /etc/php /var/lib/php /usr/share/php*
            rm -rf /usr/sbin/apache2
            apt autoremove
            apt autoclean
        fi

        echo "---------------------------------------------------------------------"
        read -p " >>> Do you want to remove mysql? (y/n): " choice
        echo "---------------------------------------------------------------------"
        if [ "$choice" == "y" ]; then
            systemctl stop mysql
            apt purge -y mysql-server mysql-client mysql-common mysql-server-core-* mysql-client-core-*
            rm -rf /etc/mysql /var/lib/mysql* /var/log/mysql /usr/share/mysql /etc/my.cnf
            apt autoremove
            apt autoclean
            apt remove -y dbconfig-mysql
        fi
    fi
}
