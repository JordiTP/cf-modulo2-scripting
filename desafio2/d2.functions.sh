#!/bin/bash

check_root() {
    if [ "$(id -u)" -eq 0 ]; then
        echo " "
        echo "---------------------------------------------------------------------"
        echo ">>> Running as root."
        echo "---------------------------------------------------------------------"
    elif pgrep -s 0 '^sudo$' > /dev/null ; then
        echo " "
        echo "---------------------------------------------------------------------"
        echo ">>> Running with sudo."
        echo "---------------------------------------------------------------------"
    else
        echo " "
        echo "---------------------------------------------------------------------"
        echo ">>> ERROR: This script must be run as root or with sudo. Exiting..."
        echo "---------------------------------------------------------------------"
        exit 1
    fi
}

edit_env() {
    echo "---------------------------------------------------------------------"
    read -p ">>> Do you want to edit the configuration file? (y/n): " choice
    echo "---------------------------------------------------------------------"
    if [ "$choice" == "y" ]; then
        read -p ">>> Which editor do you want to use? (vi/nano): " editor
        if [ "$editor" == "vi" ]; then
            if ! command -v vi &> /dev/null; then
                echo ">>> vi is not installed. Installing..."
                apt-get update
                apt-get install -y vim
            fi
            vi .env
        elif [ "$editor" == "nano" ]; then
            if ! command -v nano &> /dev/null; then
                echo ">>> nano is not installed. Installing..."
                apt-get update
                apt-get install -y nano
            fi
            nano .env
        else
            echo ">>> Invalid editor choice."
        fi
    fi
}

get_random() {
    local result=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w $1 | head -n 1)
    echo $result
}

replace_inside() {
    local result=$(sed -i "s/$1/$2/g" $3)
    echo $result
}

check_env() {
    if [ -f .env ]; then
        edit_env
    else
        echo " "
        echo "---------------------------------------------------------------------"
        echo " >>> Copying the configuration file (.env.sample to .env)"
        echo "---------------------------------------------------------------------"
        
        # Generate random values
        RANDOM_USER='wp_user_'$(get_random 4)
        RANDOM_PASSWORD=$(get_random 12)
        replace_inside "wp_user" $RANDOM_USER ".env.sample"
        replace_inside "wp_temp_pass" $RANDOM_PASSWORD ".env.sample"

        # Copy the configuration file
        cp .env.sample .env
        edit_env
    fi
}

verify_requeriments() {
    if [ "$(lsb_release -r -s)" == "20.04" ]; then
        echo " "
        echo "---------------------------------------------------------------------"
        echo " >>> Requirements satisfied:"
        echo " >>> Distibution: $(lsb_release -d | awk '{print $2, $3}')"
        echo "---------------------------------------------------------------------"
    else
        echo " "
        echo "---------------------------------------------------------------------"
        echo " >>> ERROR: Requirements not satisfied. Needs Ubuntu 20.04 . Exiting..."
        echo "---------------------------------------------------------------------"
        exit 1
    fi
}

install_lamp() {
    echo " "
    echo "---------------------------------------------------------------------"
    echo " >>> Installing LAMP on Ubuntu..."
    echo "---------------------------------------------------------------------"
    apt-get update
    apt-get install -y tasksel
    tasksel install lamp-server
    
    # Upgrade PHP to version 8.2
    add-apt-repository ppa:ondrej/php
    apt-get update
    apt-get install -y php8.2 php8.2-mysql php8.2-gd php8.2-curl php8.2-mbstring php8.2-mcrypt
    apt-get install -y php8.2-xml php8.2-xmlrpc php8.2-zip php8.2-ssh2 php8.2-intl
    update-alternatives --set php /usr/bin/php8.2

    if [ $? -eq 0 ]; then
        echo " "
        echo "---------------------------------------------------------------------"
        echo " >>> Installed successfully:"
        echo "     [L] Linux: $(lsb_release -d | awk '{print $2, $3}')"
        echo "     [A] Apache: $(dpkg-query -W -f '${version}\n' apache2)"
        echo "     [M] MySQL: $(dpkg-query -W -f '${version}\n' mysql-server)"
        echo "     [P] PHP: $(dpkg-query -W -f '${version}\n' php8.2)"
        echo "---------------------------------------------------------------------"
    else
        echo " "
        echo "---------------------------------------------------------------------"
        echo " >>> ERROR: LAMP could not be installed. Exiting..."
        echo "---------------------------------------------------------------------"
        exit 1
    fi
}

install_wordpress() {
    echo " "
    echo "---------------------------------------------------------------------"
    echo " >>> Latest WordPress release..."
    read -p " >>> Do you want WordPress in (e)English or (s)Spanish? (e/s): " choice
    echo "---------------------------------------------------------------------"
    if [ "$choice" == "e" ]; then
        wget https://www.wordpress.org/latest.tar.gz
        tar -zxvf latest.tar.gz  -C /var/www/html/
        mv /var/www/html/latest /var/www/html/wordpress
    else
        wget https://es-ar.wordpress.org/latest-es_AR.tar.gz
        tar -zxvf latest-es_AR.tar.gz  -C /var/www/html/
        mv /var/www/html/latest-es_AR /var/www/html/wordpress
    fi
    chown -R www-data:www-data /var/www/html/wordpress
}

configure_apache() {
    systemctl stop apache2
    a2dissite 000-default
    install_wordpress

    cat <<EOF > /etc/apache2/sites-available/wordpress.conf
<VirtualHost *:80>
    ServerAdmin webmaster@localhost
    DocumentRoot /var/www/html/wordpress

    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined

    <Directory /var/www/html/wordpress>
        Options FollowSymLinks
        AllowOverride All
        Require all granted
    </Directory>
</VirtualHost>
EOF

    a2ensite wordpress.conf
    systemctl restart apache2
}

configure_database() {
    systemctl start mysql
    mysql -u root <<MYSQL_SCRIPT
CREATE DATABASE $WORDPRESS_DB_NAME;
CREATE USER '$WORDPRESS_DB_USER'@'localhost' IDENTIFIED BY '$WORDPRESS_DB_PASSWORD';
GRANT ALL PRIVILEGES ON $WORDPRESS_DB_NAME.* TO '$WORDPRESS_DB_USER'@'localhost';
FLUSH PRIVILEGES;
MYSQL_SCRIPT
}

replace_inside_wp() {
    local result=$(replace_inside $1 $2 "/var/www/html/wordpress/wp-config.php")
    echo $result
}

configure_wordpress() {
    echo " "
    echo "---------------------------------------------------------------------"
    echo ">>> Configuring WordPress..."
    echo "---------------------------------------------------------------------"
    mv /var/www/html/wordpress/wp-config-sample.php /var/www/html/wordpress/wp-config.php
    
    # Replace the database values in the wp-config.php file
    replace_inside_wp "database_name_here" $WORDPRESS_DB_NAME
    replace_inside_wp "username_here" $WORDPRESS_DB_USER
    replace_inside_wp "password_here" $WORDPRESS_DB_PASSWORD
    replace_inside_wp "utf8" $WORDPRESS_DB_CHARSET
    
    # Generate random keys from API
    WPKEYS=$(curl -L https://api.wordpress.org/secret-key/1.1/salt/)
    if [ $? -ne 0 ]; then
        echo "Error occurred while retrieving WordPress secret keys from API."
        exit 1
    fi

    # Replace the keys in the wp-config.php file
    defineString='put your unique phrase here'
	printf '%s\n' "g/$defineString/d" a "$WPKEYS" . w | ed -s /var/www/html/wordpress/wp-config.php
    
    # Replace the table prefix and debug mode in the wp-config.php file
    replace_inside_wp "table_prefix = 'wp_'" $WORDPRESS_TABLE_PREFIX
    replace_inside_wp "'WP_DEBUG', false" $WORDPRESS_DEBUG

    # Reload the apache2 service
    systemctl reload apache2.service
    echo " "
    echo "---------------------------------------------------------------------"
    echo " >>> WordPress configured successfully."
    echo "---------------------------------------------------------------------"
}

replace_inside_htaccess() {
    local result=$(sed -i '/Require all denied/{N;s/Require all denied\nRequire ip MYIP/Require all granted/}' $1)
    echo $result
}

secure_wordpress() {
    echo " "
    echo "---------------------------------------------------------------------"
    echo ">>> Securing WordPress..."
    echo "---------------------------------------------------------------------"
    cp $PWD/wp.htaccess-root /var/www/html/wordpress/.htaccess
    if [ ! -d "/var/www/html/wordpress/wp-admin/" ]; then
        mkdir /var/www/html/wordpress/wp-admin/
    fi
    cp $PWD/wp.htaccess-admin /var/www/html/wordpress/wp-admin/.htaccess
    
    if [ ! -d "/var/www/html/wordpress/wp-content/uploads/" ]; then
        mkdir /var/www/html/wordpress/wp-content/uploads/
    fi
    cp $PWD/wp.htaccess-uploads /var/www/html/wordpress/wp-content/uploads/.htaccess

    # Replace the only IP address/range for access in the .htaccess files or enable all access
    read -p " >>> Insert one IP address or range, or more with a space between. Or ENTER to enable all : " iprange
    if [ "$iprange" == "" ]; then
        replace_inside_htaccess /var/www/html/wordpress/.htaccess
        replace_inside_htaccess /var/www/html/wordpress/wp-admin/.htaccess
    else
        replace_inside "MYIP" $iprange /var/www/html/wordpress/.htaccess
        replace_inside "MYIP" $iprange /var/www/html/wordpress/wp-admin/.htaccess
    fi

    # Change the permissions of the .htaccess files and wp-config.php
    chmod 644 /var/www/html/wordpress/.htaccess
    chmod 644 /var/www/html/wordpress/wp-admin/.htaccess
    chmod 644 /var/www/html/wordpress/wp-admin/.htaccess
    chmod 640 /var/www/html/wordpress/wp-config.php

    systemctl restart apache2
    echo " "
    echo "---------------------------------------------------------------------"
    echo " >>> WordPress secured successfully."
    echo "---------------------------------------------------------------------"
}

cleanup_wordpress() {
    echo "---------------------------------------------------------------------"
    echo " >>> Removing WordPress..."
    echo "---------------------------------------------------------------------"
    
    # Stop the Apache service
    systemctl stop apache2
    
    # Remove WordPress from the Apache www directory
    rm -rf /var/www/html/wordpress

    # Drop the WordPress database
    mysql -u root -p <<EOF
DROP DATABASE $WORDPRESS_DB_NAME;
DROP USER '$WORDPRESS_DB_USER'@'localhost';
EOF

    # Remove WordPress from the Apache configuration
    a2ensite 000-default
    a2dissite wordpress.conf
    rm -rf /etc/apache2/sites-available/wordpress.conf

    # Start the Apache service
    systemctl start apache2

    # Remove the downloaded files
    rm -rf $PWD/latest.tar.gz
    rm -rf $PWD/latest-es_AR.tar.gz

    echo "---------------------------------------------------------------------"
    echo " >>> WordPress has been successfully uninstalled."
    echo "---------------------------------------------------------------------"
}

cleanup_lamp() {
    echo "---------------------------------------------------------------------"
    read -p " >>> Do you want to remove LAMP? (y/n): " choice
    echo "---------------------------------------------------------------------"
    if [ "$choice" == "y" ]; then
        echo ">>> Removing LAMP on Ubuntu..."
        
        # Remove apache2
        systemctl stop apache2
        apt purge -y apache2* libapache2*
        apt remove -y apache2 libapache2*
        rm -rf /etc/apache2 /var/lib/apache2 /var/log/apache2 /usr/share/apache2 /var/www
        rm -rf /usr/sbin/apache2
        apt autoremove
        apt autoclean

        # Remove php-8.2
        systemctl stop apache2
        apt purge -y php*
        apt remove -y php*        
        rm -rf /etc/php /var/lib/php /usr/share/php*
        apt autoremove
        apt autoclean

        # Remove mysql
        systemctl stop mysql
        apt purge -y mysql-server mysql-client mysql-common mysql-server-core-* mysql-client-core-*
        rm -rf /etc/mysql /var/lib/mysql* /var/log/mysql /usr/share/mysql /etc/my.cnf
        apt autoremove
        apt autoclean
        apt remove -y dbconfig-mysql

        echo "---------------------------------------------------------------------"
        echo " >>> LAMP has been successfully uninstalled."
        echo "---------------------------------------------------------------------"
    fi
}
