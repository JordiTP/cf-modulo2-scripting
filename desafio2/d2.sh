#!/bin/bash

# Get the functions
source $PWD/d2.functions.sh

# Get the environment variables
if [ -f .env ]; then
    # Load the environment variables
    source $PWD/.env
    # Check if script is running as root
    check_root
else
    # Check if script is running as root
    check_root
    # Edit the configuration file
    check_env
    # Load the environment variables
    source $PWD/.env
fi

echo "---------------------------------------------------------------------"
echo " "
echo " >>> Challenge 2 - CF-DevOps-Roxs"
echo " >>> Install WordPress with LAMP"
echo " >>> Distibution: $(lsb_release -d | awk '{print $2, $3}')"
echo " "
echo "---------------------------------------------------------------------"
sleep 2

# Cleanup execution enable
chmod +x $PWD/cleanup.sh

# Verify the requirements
verify_requeriments

# Install LAMP
install_lamp

# Configure apache2
configure_apache

# Configure database
configure_database

# Configure WordPress
configure_wordpress

# Secure WordPress
secure_wordpress

# End of the script
echo "---------------------------------------------------------------------"
echo " "
echo " >>> Finished the script of Challenge 2 - CF-DevOps-Roxs"
echo " >>> WordPress version: $(cat /var/www/html/wordpress/wp-includes/version.php | grep 'wp_version =' | grep -o '[0-9.]\+')"
echo " "
echo " >>> Access WordPress at: "
echo "     Local: http://localhost/"
echo "     Public: http://$(hostname -I | sed 's/ $//')/"
echo " "
echo " >>> Database Name: "$WORDPRESS_DB_USER
echo " >>> Database Username: "$WORDPRESS_DB_USER
echo " >>> Database Password: "$WORDPRESS_DB_PASSWORD
echo " "
echo "---------------------------------------------------------------------"
sleep 2
