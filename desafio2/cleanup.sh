#!/bin/bash

# Get the functions
source $PWD/d2.functions.sh
source $PWD/.env

# Check if script is running as root
check_root

echo "---------------------------------------------------------------------"
echo " "
echo " >>> Challenge 2 - CF-DevOps-Roxs"
echo " >>> Cleanup the environment - Remove WordPress and LAMP"
echo " >>> Distibution: $(lsb_release -d | awk '{print $2, $3}')"
echo " "
echo "---------------------------------------------------------------------"
sleep 2

# WordPress cleanup
cleanup_wordpress

# LAMP cleanup
cleanup_lamp

# Remove the environment file
rm -rf $PWD/.env

# End of the script
echo "---------------------------------------------------------------------"
echo " "
echo " >>> Finished the cleanup of Challenge 2 - CF-DevOps-Roxs"
echo " "
echo "---------------------------------------------------------------------"
sleep 2
